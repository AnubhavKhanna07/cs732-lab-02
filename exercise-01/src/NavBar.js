import styles from './NavBar.module.css';
import { NavLink } from 'react-router-dom';

export default function NavBar({customers, setCustomers}) {

    const tiersMap = {}
    for (const customer of customers) {
        if (tiersMap[customer['membershipTier']]) {
            tiersMap[customer['membershipTier']] += 1
        }
        else {
            tiersMap[customer['membershipTier']] = 1
        }
    }

    // for (const [key, value] of Object.entries(tiersMap)) {
    //     console.log(`Tier ${key} : ${value}`)
    // }

    // console.log(tiersMap)
    // console.log(Object.entries(tiersMap))
    // console.log(Object.entries(tiersMap).reduce((total, pair) => total + tiersMap[pair[0]], 0))

    // TODO: Can improve the way I display the different tiers by hard-coding, surely there's a way.
    // Also I can improve the way the summary appears in the nav bar
    return (
        <div className={styles.navBar}>
            <NavLink to="/customers" activeClassName={styles.activeLink}>Customers</NavLink>
            <div>
                {`Customer details: Bronze: ${tiersMap['Bronze']}, Silver: ${tiersMap['Silver']}, Gold: ${tiersMap['Gold']}, Platinum: ${tiersMap['Platinum']}. `}
                <strong>Total: </strong>{Object.entries(tiersMap).reduce((total, pair) => total + tiersMap[pair[0]], 0)}
            </div>
        </div>
    );
}