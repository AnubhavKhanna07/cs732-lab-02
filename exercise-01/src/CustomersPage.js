import CustomerTable from './CustomerTable';
import { useHistory, Route, Switch, Link } from 'react-router-dom';
import NewCustomerDialog from './NewCustomerDialog';
import dayjs from 'dayjs';

export default function CustomersPage({customers, setCustomers}) {

    const history = useHistory();

    const buttonStyle = {
        float: 'right'
    }

    function onAddCustomer(firstName, lastName, dob, tier, expiryDate) {
        // Add the new customer
        const updatedCustomers = [...customers];
        const newCustomer = {
            id: customers.length + 1,
            firstName: firstName,
            lastName: lastName,
            dob: dayjs(dob),
            membershipExpires: expiryDate,
            membershipTier: tier
        };
        updatedCustomers.push(newCustomer);
        setCustomers(updatedCustomers);

        // Redirect the user
        history.replace('/customers');
    }

    function onCancelNewCustomer() {
        history.goBack();
    }

    return (
        <>
            <main>
                <Switch>
                    <Route path={'/customers'}>
                        <div className="box">
                            <CustomerTable customers={customers} />
                            <button style={buttonStyle}>
                                <Link to={'/customers/add'}>Add new customer</Link>
                            </button>
                        </div>
                    </Route>
                </Switch>

                {/* This Switch will be rendered regardless of which of the above Routes matches.
                    It will display the "add article" dialog if required. */}
                <Switch>
                    <Route path={'/customers/add'}>
                        <NewCustomerDialog onAddCustomer={onAddCustomer} onCancelNewCustomer={onCancelNewCustomer} />
                    </Route>
                </Switch>
            </main>
        </>
    )
}