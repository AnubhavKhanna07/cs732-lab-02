import { Switch, Route, Redirect, useHistory } from 'react-router-dom';
import { useState } from 'react';
import { initialCustomers } from './data';
import NavBar from './NavBar';
import CustomersPage from './CustomersPage';

/**
 * Renders a navbar allowing the user to browse to the articles or gallery pages.
 * If the user tries to browse to any other URL, they are auto-redirected to the articles page.
 */
function App() {

  const [customers, setCustomers] = useState(initialCustomers);

  return (
    <div className="container">

      <nav>
        <NavBar customers={customers} setCustomers={setCustomers}/>
      </nav>

      <Switch>
        <Route path="/customers">
          <CustomersPage customers={customers} setCustomers={setCustomers} />
        </Route>

        <Route path="*">
          <Redirect to="/customers" />
        </Route>
      </Switch>

    </div>
  );
}

export default App;