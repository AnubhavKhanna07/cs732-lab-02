import styles from './NewCustomerDialog.module.css';
import { useState } from 'react';
import Modal from './Modal';
import { Link } from 'react-router-dom'

export default function NewCustomerDialog({ onAddCustomer, onCancelNewCustomer }) {

    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [dob, setDob] = useState('');
    const [tier, setTier] = useState('');
    const [expiryDate, setExpiryDate] = useState('');

    return (
        <Modal style={{ width: '50%', height: 'auto' }} dismissOnClickOutside={true} onCancel={onCancelNewCustomer}>
            <h2>Add customer</h2>
            <div className={styles.form}>
                <div className={styles.formRow}>
                    <label>First name:</label>
                    <input type="text" value={firstName} onInput={e => setFirstName(e.target.value)} />
                </div>
                <div className={styles.formRow}>
                    <label>Last name:</label>
                    <input type="text" value={lastName} onInput={e => setLastName(e.target.value)} />
                </div>
                <div className={styles.formRow}>
                    <label>Date of birth:</label>
                    <input type="text" value={dob} onInput={e => setDob(e.target.value)} />
                </div>
                <div className={styles.formRow}>
                    <label>Tier:</label>
                    <input type="text" value={tier} onInput={e => setTier(e.target.value)} />
                </div>
                <div className={styles.formRow}>
                    <label>Expiry Date:</label>
                    <input type="text" value={expiryDate} onInput={e => setExpiryDate(e.target.value)} />
                </div>
                <div className={styles.formRow} style={{ flexDirection: 'row-reverse' }}>
                    <button
                        style={{ flexBasis: '100px', flexGrow: 0 }}
                        onClick={() => onAddCustomer(firstName, lastName, dob, tier, expiryDate)}>
                        Add
                    </button>
                </div>
            </div>
        </Modal>
    );
}